from openpyxl import load_workbook
from openpyxl import Workbook

wb_saldo = load_workbook('SaldoITEM.xlsx', read_only=True)
wb_mov = load_workbook('MovtoITEM.xlsx', read_only=True)

ws_saldo = wb_saldo.active
ws_mov = wb_mov.active

saldo_data = ws_saldo.values
mov_data = ws_mov.values

#skip header lines
mcols = next(mov_data)[0:]
scols = next(saldo_data)[0:]

# Saldo
sdata = {}
for it,di,qi,vi,df,qf,vf in saldo_data:
    sdata.setdefault(it,{"data_inicio":di,"qde_inicio":qi,"valor_inicio":vi,"data_final":df,"qde_final":qf,"valor_final":vf})

# Movimentações
ddata = {}
for it,ty,dt,qt,v in mov_data:
    ddata.setdefault(it,{'quantidade_total':0, 'values':dict()})['values'].setdefault(dt,{'list':[],'valor_inicial':0, 'valor_final': 0, 'quantidade_inicial':0,'quantidade_final':0, 'valor_ent_dia':0, 'valor_sai_dia':0, 'quantidade_ent_dia':0, 'quantidade_sai_dia':0})['list'].append({'optype': ty, 'quantidade':qt, 'valor':v})

# Síntese dos dados
for it,itval in ddata.items():
    itval['quantidade_total'] = sdata[it]["qde_inicio"]# initial value
    itval['valor_total'] = sdata[it]["valor_inicio"] # initial value

    for dt,dtval in itval['values'].items():
        listdata = dtval['list']
        dtval['quantidade_inicial'] = itval['quantidade_total']
        dtval['valor_inicial'] = itval['valor_total']
        
        dtval['quantidade_ent_dia'] = sum(x['quantidade'] for x in listdata if x['optype'] == 'Ent')
        dtval['quantidade_sai_dia'] = sum(x['quantidade'] for x in listdata if x['optype'] == 'Sai')
        quantidade_per_day =  dtval['quantidade_ent_dia'] - dtval['quantidade_sai_dia']

        dtval['valor_ent_dia'] = sum(x['valor'] for x in listdata if x['optype'] == 'Ent')
        dtval['valor_sai_dia'] = sum(x['valor'] for x in listdata if x['optype'] == 'Sai')
        valor_per_day = dtval['valor_ent_dia'] - dtval['valor_sai_dia']

        dtval['quantidade_final'] = dtval['quantidade_inicial'] + quantidade_per_day
        dtval['valor_final'] = dtval['valor_inicial'] + valor_per_day

        itval['quantidade_total'] = dtval['quantidade_final']
        itval['valor_total'] = dtval['valor_final']

# output data
wb = Workbook()
ws = wb.active

ws.cell(row=1, column=1).value = 'Item'
ws.cell(row=1, column=2).value = 'Data do lançamento'
ws.cell(row=1, column=3).value = 'Lançamentos de ENTRADA: quantidade'
ws.cell(row=1, column=4).value = 'Lançamentos de ENTRADA: valor'
ws.cell(row=1, column=5).value = 'Lançamentos de SAIDA: quantidade'
ws.cell(row=1, column=6).value = 'Lançamentos de SAIDA: quantidade'
ws.cell(row=1, column=7).value = 'Saldo Inicial em quantidade'
ws.cell(row=1, column=8).value = 'Saldo Inicial em valor'
ws.cell(row=1, column=9).value = 'Saldo Final em quantidade'
ws.cell(row=1, column=10).value = 'Saldo Final em valor'
n = 2
for it,itval in ddata.items():
    for dt,dtval in itval['values'].items():
        ws.cell(row=n, column=1).value = str(it)
        ws.cell(row=n, column=2).value = str('%02d' % dt.day)+'/'+str('%02d' % dt.month)+'/'+str(dt.year)
        ws.cell(row=n, column=3).value = str('%.2f' % dtval['quantidade_ent_dia'])
        ws.cell(row=n, column=4).value = str('%.2f' % dtval['valor_ent_dia'])
        ws.cell(row=n, column=5).value = str('%.2f' % dtval['quantidade_sai_dia'])
        ws.cell(row=n, column=6).value = str('%.2f' % dtval['valor_sai_dia'])
        ws.cell(row=n, column=7).value = str('%.2f' % dtval['quantidade_inicial'])
        ws.cell(row=n, column=8).value = str('%.2f' % dtval['valor_inicial'])
        ws.cell(row=n, column=9).value = str('%.2f' % dtval['quantidade_final'])
        ws.cell(row=n, column=10).value = str('%.2f' % dtval['valor_final'])
        n = n + 1

wb.save('summary.xlsx')